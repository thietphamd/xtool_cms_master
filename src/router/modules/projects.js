import Layout from '@/layout'

const projectRoute = {
  path: '/projects',
  component: Layout,
  redirect: 'noRedirect',
  name: 'Projects',
  // hidden: true,
  meta: {
    title: 'Projects',
    icon: 'el-icon-s-cooperation'
  },
  children: [
    {
      path: 'texture',
      component: () => import('@/views/projects/texture'),
      name: 'projects.texture',
      meta: { title: 'Texture', noCache: true }
    },
    {
      path: 'material',
      component: () => import('@/views/projects/material'),
      name: 'Material',
      meta: { title: 'Material', noCache: true }
    },
    {
      path: 'model3d',
      component: () => import('@/views/projects/model3d'),
      name: 'projects.model3d',
      meta: { title: 'Model 3D', noCache: true }
    },
    {
      path: 'product',
      component: () => import('@/views/projects/product'),
      name: 'projects.product',
      meta: { title: 'Product 360 Request', noCache: true }
    }
  ]
}

export default projectRoute
