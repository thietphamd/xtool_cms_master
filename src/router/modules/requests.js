import Layout from '@/layout'

const billingRoute = {
  path: '/request',
  component: Layout,
  redirect: '/request',
  children: [
    {
      path: '/request',
      component: () => import('@/views/requests/index'),
      name: 'Requests',
      meta: {
        title: 'Requests',
        icon: 'el-icon-chat-dot-round'
      }
    }
  ]
}

export default billingRoute
