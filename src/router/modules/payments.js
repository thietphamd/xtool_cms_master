import Layout from '@/layout'

const paymentRoute = {
  path: '/payments',
  component: Layout,
  redirect: '/payment',
  children: [
    {
      path: '/payment',
      component: () => import('@/views/payments/index'),
      name: 'Payments',
      meta: {
        title: 'Payments',
        icon: 'el-icon-menu'
      }
    }
  ]
}

export default paymentRoute
