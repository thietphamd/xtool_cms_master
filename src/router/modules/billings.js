import Layout from '@/layout'

const billingRoute = {
  path: '/payment',
  component: Layout,
  redirect: '/payment',
  children: [
    {
      path: '/payment',
      component: () => import('@/views/billings/index'),
      name: 'Payments',
      meta: {
        title: 'Payments',
        icon: 'el-icon-s-order'
      }
    }
  ]
}

export default billingRoute
