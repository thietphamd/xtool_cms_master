import Layout from '@/layout'

const subscriptionRoute = {
      path: '/subscription',
      //component: () => import('@/views/configurations/subscription/index'), // Parent router-view
      name: 'Subscription',
      component: Layout,
      meta: { title: 'Subscriptions', icon: 'el-icon-s-tools' },
      redirect: 'noRedirect',
      children: [
        {
          path: 'plans',
          component: () => import('@/views/subscriptions/subscription/plans'),
          name: 'subscription.plans',
          meta: { title: 'Plans' }
        },
        
        {
          path: 'applications',
          component: () => import('@/views/subscriptions/subscription/applications'),
          name: 'subscription.applications',
          meta: { title: 'Applications' }
        }
      ]
}

export default subscriptionRoute
