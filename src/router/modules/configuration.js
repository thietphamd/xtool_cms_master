/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const configurationRouter = {
  path: '/configuration',
  component: Layout,
  redirect: '/configuration/subcription/plans',
  name: 'Configuration',
  meta: {
    title: 'Configuration',
    icon: 'el-icon-s-tools'
  },
  children: [
    {
      path: 'subscription',
      component: () => import('@/views/configurations/subscription/index'), // Parent router-view
      name: 'Subscription',
      meta: { title: 'Subscription Config' },
      redirect: '/configuration/subcription/plans',
      children: [
        {
          path: 'plans',
          component: () => import('@/views/configurations/subscription/plans'),
          name: 'configuration.plans',
          meta: { title: 'Plans' }
        },
        // {
        //   path: 'features',
        //   component: () => import('@/views/configurations/subscription/features'),
        //   name: 'configuration.features',
        //   meta: { title: 'Features' }
        // },
        {
          path: 'applications',
          component: () => import('@/views/configurations/subscription/applications'),
          name: 'configuration.applications',
          meta: { title: 'Applications' }
        },
        // {
        //   path: 'options',
        //   component: () => import('@/views/configurations/subscription/options'),
        //   name: 'configuration.options',
        //   meta: { title: 'Options' }
        // },
        // {
        //   path: 'offers',
        //   component: () => import('@/views/configurations/subscription/offers'),
        //   name: 'configuration.offers',
        //   meta: { title: 'Offers' }
        // }
      ]
    },
    {
      path: 'billing',
      component: () => import('@/views/configurations/billing/index'), // Parent router-view
      name: 'Billing',
      meta: { title: 'Billing' },
      redirect: '/configuration/billing/countries',
      hidden: true,
      children: [
        {
          path: 'countries',
          component: () => import('@/views/configurations/billing/countries'),
          name: 'configuration.countries',
          meta: { title: 'Countries' }
        },
        {
          path: 'payment-methods',
          component: () => import('@/views/configurations/billing/payment-methods'),
          name: 'configuration.paymentMethods',
          meta: { title: 'Payment Methods' }
        },
        {
          path: 'payment-providers',
          component: () => import('@/views/configurations/billing/payment-providers'),
          name: 'configuration.paymentProvider',
          meta: { title: 'Payment Providers' }
        }
      ]
    }
  ]
}

export default configurationRouter
