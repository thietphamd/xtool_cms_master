import Layout from '@/layout'

const billingRoute = {
  path: '/invoices',
  component: Layout,
  redirect: '/invoice',
  children: [
    {
      path: '/invoice',
      component: () => import('@/views/invoices/index'),
      name: 'Invoices',
      meta: {
        title: 'Invoices',
        icon: 'el-icon-s-order'
      }
    }
  ]
}

export default billingRoute
