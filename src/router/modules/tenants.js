import Layout from '@/layout'

const billingRoute = {
  path: '/tenant',
  component: Layout,
  redirect: '/tenant',
  children: [
    {
      path: '/tenant',
      component: () => import('@/views/tenants/index'),
      name: 'Tenants',
      meta: {
        title: 'Tenants',
        icon: 'el-icon-user'
      }
    }
  ]
}

export default billingRoute
