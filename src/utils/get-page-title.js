import defaultSettings from '@/settings'

const title = defaultSettings.title || 'CMS Admin | Fitin 3D'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
