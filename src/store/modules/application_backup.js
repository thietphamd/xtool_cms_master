import { FitinServices } from '@/core'
import { HTTP_STATUS_CODE } from '@/core/constants'

const state = {
  list: [],
  current: {}
}

const mutations = {
  SET_LIST: (state, list) => {
    state.list = list
  },
  SET_APPLICATION: (state, application) => {
    state.current = application
  }
}

const actions = {
  async getList({ commit }) {
    const response = await FitinServices.ApplicationsService.getListPublic()
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      const data = response.data || {}
      commit('SET_LIST', data.content || [])
    }
  },
  async create({ commit, dispatch }, data) {
    const response = await FitinServices.ApplicationsService.create(data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      dispatch('getList')
    }

    return response
  },
  async update({ dispatch }, data) {
    const response = await FitinServices.ApplicationsService.update(data.id, data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      dispatch('getList')
    }
    return response
  },
  async delete({ dispatch }, data) {
    const id = data.id || data
    const response = await FitinServices.ApplicationsService.remove(id)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      dispatch('getList')
    }
    return response
  },
  async getDetail({ commit }, data) {
    const id = data.id || data
    const response = await FitinServices.ApplicationsService.getById(id)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      const result = response.data || {}
      commit('SET_APPLICATION', result)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
