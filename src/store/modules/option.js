import { FitinServices } from '@/core'
import { HTTP_STATUS_CODE } from '@/core/constants'

const state = {
  list: []
}

const mutations = {
  SET_LIST: (state, list) => {
    state.list = list
  }
}

const actions = {
  async getList({ commit }) {
    const response = await FitinServices.OptionsService.getListPublic()
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      const data = response.data || {}
      commit('SET_LIST', data.content || [])
    }
  },
  async create({ commit, dispatch }, data) {
    const response = await FitinServices.OptionsService.create(data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      dispatch('getList')
    }

    return response
  },
  async update({ dispatch }, data) {
    const response = await FitinServices.OptionsService.update(data.id, data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      dispatch('getList')
    }
    return response
  },
  async delete({ dispatch }, data) {
    const id = data.id || data
    const response = await FitinServices.OptionsService.remove(id)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      dispatch('getList')
    }
    return response
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
