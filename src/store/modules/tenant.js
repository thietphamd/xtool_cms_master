import { FitinServices } from '@/core'
import { login, logout, getInfo } from '@/api/user'
import { getList, create, update, destroy } from '@/api/tenant'
import { HTTP_STATUS_CODE } from '@/core/constants'
import Vue from 'vue'
const state = {
  list: []
}

const mutations = {
  SET_LIST: (state, list) => {
    state.list = list
  }
}

const actions = {
  getList({ commit }) {
    // const response = await FitinServices.PlansService.getListPublic()
    // if (response.status === HTTP_STATUS_CODE.SUCCESS) {
    //   const data = response.data || {}
    //   commit('SET_LIST', data.content || [])
    // } else {
    //   Vue.prototype.$message({
    //     message: 'Oops, Get plan error!',
    //     type: 'error'
    //   })
    // }


    
      return new Promise( (resolve, reject) => {
        getList().then(response => {
          if (response.code === 0) {
               const data = response.data || {}
               commit('SET_LIST', data.list || [])
               
             } else {
               Vue.prototype.$message({
                 message: 'Oops, Get plan error!',
                 type: 'error'
             });
            }
          resolve()
        }).catch(err => {
          reject(err)
        })
      })

  },

  create({ commit, dispatch }, data){
    return new Promise( (resolve, reject) => {
      create(data).then(response => {
        if (response.code === 0) {
          console.log('data', data, dispatch);
             const data = response.data || {}
            
             
           } else {
             Vue.prototype.$message({
               message: 'Oops, Get plan error!',
               type: 'error'
           });
          }
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  },


  async update({ commit, dispatch }, data) {
    return new Promise( (resolve, reject) => {
      update(data).then(response => {
        if (response.code === 0) {
             const data = response.data || {}
            // commit('SET_LIST', data.list || [])
             
           } else {
             Vue.prototype.$message({
               message: 'Oops, Get plan error!',
               type: 'error'
           });
          }
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  },

  async delete({ commit, dispatch }, data) {
    return new Promise( (resolve, reject) => {
      destroy(data).then(response => {
        if (response.code === 0) {
             const data = response.data || {}
            // commit('SET_LIST', data.list || [])
             
           } else {
             Vue.prototype.$message({
               message: 'Oops, Get plan error!',
               type: 'error'
           });
          }
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  },
  // async create({ commit, dispatch }, data) {
  //   const response = await FitinServices.PlansService.create(data)
  //   if (response.status === HTTP_STATUS_CODE.SUCCESS) {
  //     Vue.prototype.$message({
  //       message: 'Create plan success',
  //       type: 'success'
  //     })
  //     dispatch('getList')
  //   } else {
  //     Vue.prototype.$message({
  //       message: 'Oops, Create plan error!',
  //       type: 'error'
  //     })
  //   }

  //   return response
  // },
  // async update({ dispatch }, data) {
  //   const response = await FitinServices.PlansService.update(data.id, data)
  //   if (response.status === HTTP_STATUS_CODE.SUCCESS) {
  //     Vue.prototype.$message({
  //       message: 'Update plan success',
  //       type: 'success'
  //     })
  //     dispatch('getList')
  //   } else {
  //     Vue.prototype.$message({
  //       message: 'Oops, Update plan Error!',
  //       type: 'error'
  //     })
  //   }
  //   return response
  // },
  // async delete({ dispatch }, data) {
  //   const id = data.id || data
  //   const response = await FitinServices.PlansService.remove(id)
  //   if (response.status === HTTP_STATUS_CODE.SUCCESS) {
  //     dispatch('getList')
  //     Vue.prototype.$message({
  //       message: 'Delete plan success',
  //       type: 'success'
  //     })
  //   } else {
  //     Vue.prototype.$message({
  //       message: 'Oops, Delete plan error!',
  //       type: 'error'
  //     })
  //   }
  //   return response
  // }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
