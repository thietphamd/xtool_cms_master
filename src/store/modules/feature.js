import { FitinServices } from '@/core'
import { HTTP_STATUS_CODE } from '@/core/constants'
import Vue from 'vue'
const state = {
  list: []
}

const mutations = {
  SET_LIST: (state, list) => {
    state.list = list
  }
}

const actions = {
  async getList({ commit }) {
    const response = await FitinServices.FeaturesService.getListPublic()
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      const data = response.data || {}
      commit('SET_LIST', data.content || [])
    } else {
      Vue.prototype.$message({
        message: 'Oops, Get feature error!',
        type: 'error'
      })
    }
  },
  async create({ commit, dispatch }, data) {
    const response = await FitinServices.FeaturesService.create(data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      Vue.prototype.$message({
        message: 'Create feature success',
        type: 'success'
      })
      dispatch('getList')
    } else {
      Vue.prototype.$message({
        message: 'Oops, Create feature error!',
        type: 'error'
      })
    }

    return response
  },
  async update({ dispatch }, data) {
    const response = await FitinServices.FeaturesService.update(data.id, data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      Vue.prototype.$message({
        message: 'Update feature success',
        type: 'success'
      })
      dispatch('getList')
    } else {
      Vue.prototype.$message({
        message: 'Oops, Update feature Error!',
        type: 'error'
      })
    }
    return response
  },
  async delete({ dispatch }, data) {
    const id = data.id || data
    const response = await FitinServices.FeaturesService.remove(id)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      dispatch('getList')
      Vue.prototype.$message({
        message: 'Delete feature success',
        type: 'success'
      })
    } else {
      Vue.prototype.$message({
        message: 'Oops, Delete feature error!',
        type: 'error'
      })
    }
    return response
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
