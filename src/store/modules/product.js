import { FitinServices } from '@/core'
import { HTTP_STATUS_CODE } from '@/core/constants'
import Vue from 'vue'
const state = {
  list: [],
  list360Requested: []
}

const mutations = {
  SET_LIST: (state, list) => {
    state.list = list
  },
  SET_LIST_360_REQUEST: (state, list) => {
    state.list360Requested = list || []
  }
}

const actions = {
  async getList360Requested({ commit }) {
    const response = await FitinServices.ProductService.getList360Requested()
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      const data = response.data || []
      commit('SET_LIST_360_REQUEST', data || [])
    } else {
      Vue.prototype.$message({
        message: 'Oops, Get product request 360 error!',
        type: 'error'
      })
    }
  },
  async upload360({ dispatch }, data) {
    const id = data.data.id || data.data.get('id')
    if (!id) return
    const response = await FitinServices.ProductService.upload360(data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      dispatch('getList360Requested')
      Vue.prototype.$message({
        message: 'Upload images for product success',
        type: 'success'
      })
    } else {
      Vue.prototype.$message({
        message: 'Oops, Upload images for product error!',
        type: 'error'
      })
    }
    return response
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
