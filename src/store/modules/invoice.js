
import { getList, create, update, destroy, detail, download } from '@/api/invoice'
import Vue from 'vue'
const state = {
  list: [],
  current: {}
}

const mutations = {
  SET_LIST: (state, list) => {
    state.list = list
  },
  SET_DETAIL: (state, detail) => {
    state.current = detail
  }
}

const actions = {
  getList({ commit }) {
    
      return new Promise( (resolve, reject) => {
        getList().then(response => {
          if (response.code === 0) {
               const data = response.data || {}
               commit('SET_LIST', data.list || [])
               
             } else {
               Vue.prototype.$message({
                 message: 'Oops, error!',
                 type: 'error'
             });
            }
          resolve()
        }).catch(err => {
          reject(err)
        })
      })

  },

  getDetail({ commit, dispatch }, id) {
    return new Promise( (resolve, reject) => {
      detail(id).then(response => {
        if (response.code === 0) {
             const data = response.data || {}
            commit('SET_DETAIL', data || {})
             
           } else {
             Vue.prototype.$message({
               message: 'Oops, Get detail error!',
               type: 'error'
           });
          }
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  },

  create({ commit, dispatch }, data){
    return new Promise( (resolve, reject) => {
      create(data).then(response => {
        if (response.code === 0) {
          console.log('data', data, dispatch);
             const data = response.data || {}
            
             
           } else {
             Vue.prototype.$message({
               message: 'Oops, Get error!',
               type: 'error'
           });
          }
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  },


  update({ commit, dispatch }, data) {
    return new Promise( (resolve, reject) => {
      update(data).then(response => {
        if (response.code === 0) {
             const data = response.data || {}
            // commit('SET_LIST', data.list || [])
             
           } else {
             Vue.prototype.$message({
               message: 'Oops, Get error!',
               type: 'error'
           });
          }
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  },

  delete({ commit, dispatch }, data) {
    return new Promise( (resolve, reject) => {
      destroy(data).then(response => {
        if (response.code === 0) {
             const data = response.data || {}
            // commit('SET_LIST', data.list || [])
             
           } else {
             Vue.prototype.$message({
               message: 'Oops, Get error!',
               type: 'error'
           });
          }
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  },
  
  download({ commit, dispatch }, id) {
    return new Promise( (resolve, reject) => {
      download(id).then(response => {
        resolve(response)
      }).catch(err => {
        reject(err)
      })
    })
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
