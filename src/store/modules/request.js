import { FitinServices } from '@/core'
import { getList, detail, updateReject, updateFinish, destroy } from '@/api/request'
import { HTTP_STATUS_CODE } from '@/core/constants'
import Vue from 'vue'
const state = {
  list: []
}

const mutations = {
  SET_LIST: (state, list) => {
    state.list = list
  }
}

const actions = {
  getList({ commit }) {

      return new Promise( (resolve, reject) => {
        getList().then(response => {
          if (response.code === 0) {
               const data = response.data || {}
               commit('SET_LIST', data.list || [])
               
             } else {
               Vue.prototype.$message({
                 message: 'Oops, Get plan error!',
                 type: 'error'
             });
            }
          resolve()
        }).catch(err => {
          reject(err)
        })
      })

  },

  getDetail({ commit, dispatch }, id) {
    return new Promise( (resolve, reject) => {
      detail(id).then(response => {
        if (response.code === 0) {
             const data = response.data || {}
            commit('SET_DETAIL', data || {})
             
           } else {
             Vue.prototype.$message({
               message: 'Oops, Get detail error!',
               type: 'error'
           });
          }
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  },


  async updateReject({ commit, dispatch }, data) {
    return new Promise( (resolve, reject) => {
      updateReject(data).then(response => {
        if (response.code === 0) {
             const data = response.data || {}
            // commit('SET_LIST', data.list || [])
             
           } else {
             Vue.prototype.$message({
               message: 'Oops, Get plan error!',
               type: 'error'
           });
          }
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  },

  async updateFinish({ commit, dispatch }, data) {
    return new Promise( (resolve, reject) => {
      updateFinish(data).then(response => {
        if (response.code === 0) {
             const data = response.data || {}
            // commit('SET_LIST', data.list || [])
             
           } else {
             Vue.prototype.$message({
               message: 'Oops, Get plan error!',
               type: 'error'
           });
          }
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  },

  async delete({ commit, dispatch }, data) {
    return new Promise( (resolve, reject) => {
      destroy(data).then(response => {
        if (response.code === 0) {
             const data = response.data || {}
            // commit('SET_LIST', data.list || [])
             
           } else {
             Vue.prototype.$message({
               message: 'Oops, Get plan error!',
               type: 'error'
           });
          }
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  },

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
