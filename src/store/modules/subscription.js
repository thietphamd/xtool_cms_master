import { FitinServices } from '@/core'
import { HTTP_STATUS_CODE } from '@/core/constants'
import Vue from 'vue'
const state = {
  list: []
}

const mutations = {
  SET_LIST: (state, list) => {
    state.list = list
  }
}

const actions = {
  async getList({ commit }) {
    const response = await FitinServices.SubscriptionsService.getListPublic()
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      const data = response.data || {}
      commit('SET_LIST', data.content || [])
    } else {
      Vue.prototype.$message({
        message: 'Oops, Get subscription error!',
        type: 'error'
      })
    }
  },
  async create({ commit, dispatch }, data) {
    const response = await FitinServices.SubscriptionsService.create(data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      Vue.prototype.$message({
        message: 'Create subscription success',
        type: 'success'
      })
      dispatch('getList')
    } else {
      Vue.prototype.$message({
        message: 'Oops, Create subscription error!',
        type: 'error'
      })
    }

    return response
  },
  async update({ dispatch }, data) {
    const response = await FitinServices.SubscriptionsService.update(data.id, data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      Vue.prototype.$message({
        message: 'Update subscription success',
        type: 'success'
      })
      dispatch('getList')
    } else {
      Vue.prototype.$message({
        message: 'Oops, Update subscription Error!',
        type: 'error'
      })
    }
    return response
  },
  async delete({ dispatch }, data) {
    const id = data.id || data
    const response = await FitinServices.SubscriptionsService.remove(id)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      dispatch('getList')
      Vue.prototype.$message({
        message: 'Delete subscription success',
        type: 'success'
      })
    } else {
      Vue.prototype.$message({
        message: 'Oops, Delete subscription error!',
        type: 'error'
      })
    }
    return response
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
