import { FitinServices } from '@/core'
import { HTTP_STATUS_CODE } from '@/core/constants'

const state = {
  list: [],
  current: {}
}

const mutations = {
  SET_LIST: (state, list) => {
    state.list = list
  },
  SET_OFFER: (state, offer) => {
    state.current = offer
  }
}

const actions = {
  async getList({ commit }) {
    const response = await FitinServices.OffersService.getListPublic()
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      const data = response.data || {}
      commit('SET_LIST', data.content || [])
    }
  },
  async create({ commit, dispatch }, data) {
    const response = await FitinServices.OffersService.create(data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      dispatch('getList')
    }

    return response
  },
  async update({ dispatch }, data) {
    const response = await FitinServices.OffersService.update(data.id, data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      dispatch('getList')
    }
    return response
  },
  async delete({ dispatch }, data) {
    const id = data.id || data
    const response = await FitinServices.OffersService.remove(id)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      dispatch('getList')
    }
    return response
  },
  async getDetail({ commit }, data) {
    const id = data.id || data
    const response = await FitinServices.OffersService.getById(id)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      const data = response.data || {}
      commit('SET_OFFER', data)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
