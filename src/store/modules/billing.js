import { FitinServices } from '@/core'
import { HTTP_STATUS_CODE } from '@/core/constants'
import Vue from 'vue'
const state = {
  list: []
}

const mutations = {
  SET_LIST: (state, list) => {
    state.list = list
  }
}

const actions = {
  async getList({ commit }) {
    const response = await FitinServices.BillingsService.getAllBilling()
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      const data = response.data || {}
      commit('SET_LIST', data.content || response.data || [])
    } else {
      Vue.prototype.$message({
        message: 'Oops, Get bills error!',
        type: 'error'
      })
    }
  },
  async getListByTenant({ commit }) {
    const response = await FitinServices.BillingsService.getBillingsByTenantId()
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      const data = response.data || {}
      commit('SET_LIST', data.content || response.data || [])
    } else {
      Vue.prototype.$message({
        message: 'Oops, Get bills error!',
        type: 'error'
      })
    }
  },
  async create({ commit, dispatch }, data) {
    const response = await FitinServices.BillingsService.create(data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      Vue.prototype.$message({
        message: 'Create bill success',
        type: 'success'
      })
      dispatch('getList')
    } else {
      Vue.prototype.$message({
        message: 'Oops, Create bill error!',
        type: 'error'
      })
    }

    return response
  },
  async update({ dispatch }, data) {
    const response = await FitinServices.BillingsService.update(data.id, data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      Vue.prototype.$message({
        message: 'Update bill success',
        type: 'success'
      })
      dispatch('getList')
    } else {
      Vue.prototype.$message({
        message: 'Oops, Update bill Error!',
        type: 'error'
      })
    }
    return response
  },
  async delete({ dispatch }, data) {
    const id = data.id || data
    const response = await FitinServices.BillingsService.remove(id)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      dispatch('getList')
      Vue.prototype.$message({
        message: 'Delete bill success',
        type: 'success'
      })
    } else {
      Vue.prototype.$message({
        message: 'Oops, Delete bill error!',
        type: 'error'
      })
    }
    return response
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
