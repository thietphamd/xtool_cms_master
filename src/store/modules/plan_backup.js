import { FitinServices } from '@/core'
import { HTTP_STATUS_CODE } from '@/core/constants'
import Vue from 'vue'
const state = {
  list: []
}

const mutations = {
  SET_LIST: (state, list) => {
    state.list = list
  }
}

const actions = {
  async getList({ commit }) {
    const response = await FitinServices.PlansService.getListPublic()
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      const data = response.data || {}
      commit('SET_LIST', data.content || [])
    } else {
      Vue.prototype.$message({
        message: 'Oops, Get plan error!',
        type: 'error'
      })
    }
  },
  async create({ commit, dispatch }, data) {
    const response = await FitinServices.PlansService.create(data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      Vue.prototype.$message({
        message: 'Create plan success',
        type: 'success'
      })
      dispatch('getList')
    } else {
      Vue.prototype.$message({
        message: 'Oops, Create plan error!',
        type: 'error'
      })
    }

    return response
  },
  async update({ dispatch }, data) {
    const response = await FitinServices.PlansService.update(data.id, data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      Vue.prototype.$message({
        message: 'Update plan success',
        type: 'success'
      })
      dispatch('getList')
    } else {
      Vue.prototype.$message({
        message: 'Oops, Update plan Error!',
        type: 'error'
      })
    }
    return response
  },
  async delete({ dispatch }, data) {
    const id = data.id || data
    const response = await FitinServices.PlansService.remove(id)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      dispatch('getList')
      Vue.prototype.$message({
        message: 'Delete plan success',
        type: 'success'
      })
    } else {
      Vue.prototype.$message({
        message: 'Oops, Delete plan error!',
        type: 'error'
      })
    }
    return response
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
