import { FitinServices } from '@/core'
import { HTTP_STATUS_CODE } from '@/core/constants'
import Vue from 'vue'
const state = {
  list: []
}

const mutations = {
  SET_LIST: (state, list) => {
    state.list = list
  }
}

const actions = {
  async getList({ commit }) {
    const response = await FitinServices.ProjectsService.getListPublic()
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      const data = response.data || {}
      commit('SET_LIST', data.content || [])
    } else {
      Vue.prototype.$message({
        message: 'Oops, Get project error!',
        type: 'error'
      })
    }
  },
  async create({ commit, dispatch }, data) {
    const response = await FitinServices.ProjectsService.create(data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      Vue.prototype.$message({
        message: 'Create project success',
        type: 'success'
      })
      dispatch('getList')
    } else {
      Vue.prototype.$message({
        message: 'Oops, Create project error!',
        type: 'error'
      })
    }

    return response
  },
  async update({ dispatch }, data) {
    const response = await FitinServices.ProjectsService.update(data.id, data)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      Vue.prototype.$message({
        message: 'Update project success',
        type: 'success'
      })
      dispatch('getList')
    } else {
      Vue.prototype.$message({
        message: 'Oops, Update project Error!',
        type: 'error'
      })
    }
    return response
  },
  async delete({ dispatch }, data) {
    const id = data.id || data
    const response = await FitinServices.ProjectsService.remove(id)
    if (response.status === HTTP_STATUS_CODE.SUCCESS) {
      dispatch('getList')
      Vue.prototype.$message({
        message: 'Delete project success',
        type: 'success'
      })
    } else {
      Vue.prototype.$message({
        message: 'Oops, Delete project error!',
        type: 'error'
      })
    }
    return response
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
