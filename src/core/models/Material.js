import BaseModel from './BaseModel'

export default class MaterialModel extends BaseModel {
  get schema() {
    return {
      id: undefined,
      name: undefined,
      tenantId: undefined,
      dataJson: undefined,
      createdAt: undefined,
      updatedAt: undefined,
    }
  }

  prepareCreate() {
    this.baseClear()

    return this
  }

  prepareUpdate() {
    this.baseClear()

    return this
  }
}
