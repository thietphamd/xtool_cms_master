import BaseModel from './BaseModel'

export default class CategoryModel extends BaseModel {
  get schema() {
    return {
      id: undefined,
      name: undefined,
      parentId: undefined,
      tenantId: undefined,

      type: undefined,

      createdAt: undefined,
      updatedAt: undefined
    }
  }

  prepareCreate() {
    this.baseClear()

    return this
  }

  prepareUpdate() {
    this.baseClear()

    return this
  }
}
