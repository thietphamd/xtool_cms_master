export default {
  TEXTURE: 'texture',
  MODEL3D: 'model3d',
  MATERIAL: 'material',
  PRODUCT: 'product',
}
