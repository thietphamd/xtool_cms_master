export default [
  {
    code: 200,
    message: 'All done. Request successfully executed',
  },
  {
    code: 201,
    message: 'Data successfully created',
  },
  {
    code: 400,
    message: 'Bad Request',
  },
  {
    code: 401,
    message: 'Need auth',
  },
  {
    code: 404,
    message: 'Not found',
  },
  {
    code: 503,
    message: 'Service unavailable. Try again later',
  },
]
