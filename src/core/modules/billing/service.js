import axios from 'axios'
import { ResponseWrapper } from '@/core/services/util'
import { BaseService } from '@/core/services/base.service'

const BASE_URL = process.env.VUE_APP_BASE_API
export class BillingsService {
  static async createRecurringPaymentRequest(planId) {
    const response = await axios.post(
      `${BASE_URL}/billing/payment/recurring/requests`,
      { planId },
      {
        headers: {
          'X-tenant-ID': ''
        }
      }
    )
    return response.data.data
  }
  static async getAllBilling() {
    try {
      const response = await axios.get(`${BASE_URL}billing/payment/requests`, {}, {})
      return new ResponseWrapper(response, response.data.data)
    } catch (error) {
      return error.response
    }
  }
  static async getBillingsByTenantId() {
    try {
      const response = BaseService.request({ auth: true }).get(
        `${BASE_URL}billing/payment/requests`
      )
      return new ResponseWrapper(response, response.data.data)
    } catch (error) {
      return error.response
    }
  }
}
