import BaseModel from '../../../models/BaseModel'

export default class Billing extends BaseModel {
  get schema() {
    return {
      id: undefined,
      name: undefined,
      description: String,
      subscriptionId: String,
      createdAt: undefined,
      updatedAt: undefined
    }
  }

  prepareCreate() {
    this.baseClear()

    return this
  }

  prepareUpdate() {
    this.baseClear()

    return this
  }
}
