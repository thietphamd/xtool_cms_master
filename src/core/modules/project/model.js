import BaseModel from '../../../models/BaseModel'

export default class ProjectModel extends BaseModel {
  get schema() {
    return {
      id: undefined,
      name: undefined,
      description: undefined,
      tenantId: undefined,
      createdAt: undefined,
      updatedAt: undefined,
    }
  }

  prepareCreate() {
    this.baseClear()

    return this
  }

  prepareUpdate() {
    this.baseClear()

    return this
  }
}
