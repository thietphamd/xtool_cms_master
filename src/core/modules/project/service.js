import { BaseService } from '../../services/base.service'

export class ProjectsService extends BaseService {
  static get entity() {
    return 'projects'
  }
}
