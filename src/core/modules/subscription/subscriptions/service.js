import { BaseService } from '../../../services/base.service'

export class SubscriptionsService extends BaseService {
  static get entity() {
    return 'subscription/subscriptions'
  }
}
