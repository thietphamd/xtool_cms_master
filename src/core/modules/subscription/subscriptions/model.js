import BaseModel from '../../../models/BaseModel'

export default class SubscriptionModel extends BaseModel {
  get schema() {
    return {
      id: undefined,
      name: undefined,
      description: String,
      planId: String,
      offerId: String,
      startTime: Date,
      endTime: Date,
      createdAt: undefined,
      updatedAt: undefined
    }
  }

  prepareCreate() {
    this.baseClear()

    return this
  }

  prepareUpdate() {
    this.baseClear()

    return this
  }
}
