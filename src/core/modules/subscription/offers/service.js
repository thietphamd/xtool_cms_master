import { BaseService } from '../../../services/base.service'

export class OffersService extends BaseService {
  static get entity() {
    return 'subscription/offers'
  }
}
