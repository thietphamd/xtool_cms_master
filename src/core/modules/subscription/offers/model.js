import BaseModel from '../../../models/BaseModel'

export default class OffersModel extends BaseModel {
  get schema() {
    return {
      id: undefined,
      name: undefined,
      description: String,
      price: Number,
      startDate: Date,
      endDate: Date,
      discountValue: Number,
      discountType: 0,
      duration: 1,
      createdAt: undefined,
      updatedAt: undefined
    }
  }

  prepareCreate() {
    this.baseClear()

    return this
  }

  prepareUpdate() {
    this.baseClear()

    return this
  }

  getDatetimeRange() {
    return [this.startDate, this.endDate]
  }
}
