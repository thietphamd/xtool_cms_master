import { BaseService } from '../../../services/base.service'

export class ApplicationsService extends BaseService {
  static get entity() {
    return 'subscription/applications'
  }
}
