import BaseModel from '../../../models/BaseModel'

export default class PlanModel extends BaseModel {
  get schema() {
    return {
      id: undefined,
      name: undefined,
      description: String,
      options: Array,
      applications: Array,
      offers: Array,
      createdAt: undefined,
      updatedAt: undefined
    }
  }

  prepareCreate() {
    this.baseClear()

    return this
  }

  prepareUpdate() {
    this.baseClear()

    return this
  }
}
