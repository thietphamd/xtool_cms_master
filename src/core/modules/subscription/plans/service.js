import { BaseService } from '../../../services/base.service'

export class PlansService extends BaseService {
  static get entity() {
    return 'subscription/plans'
  }
}
