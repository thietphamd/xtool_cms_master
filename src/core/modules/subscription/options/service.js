import { BaseService } from '../../../services/base.service'

export class OptionsService extends BaseService {
  static get entity() {
    return 'subscription/options'
  }
}
