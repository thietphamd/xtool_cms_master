import BaseModel from '../../../models/BaseModel'

export default class OptionModel extends BaseModel {
  get schema() {
    return {
      id: undefined,
      name: undefined,
      description: String,
      price: Number,
      createdAt: undefined,
      updatedAt: undefined
    }
  }

  prepareCreate() {
    this.baseClear()

    return this
  }

  prepareUpdate() {
    this.baseClear()

    return this
  }
}
