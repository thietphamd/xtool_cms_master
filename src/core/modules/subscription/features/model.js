import BaseModel from '../../../models/BaseModel'

export default class FeatureModel extends BaseModel {
  get schema() {
    return {
      id: undefined,
      name: undefined,
      description: String,
      featureIds: Array,
      createdAt: undefined,
      updatedAt: undefined
    }
  }

  prepareCreate() {
    this.baseClear()

    return this
  }

  prepareUpdate() {
    this.baseClear()

    return this
  }
}
