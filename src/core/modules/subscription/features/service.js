import { BaseService } from '../../../services/base.service'

export class FeaturesService extends BaseService {
  static get entity() {
    return 'subscription/features'
  }
}
