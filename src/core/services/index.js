import { AuthService } from './auth.service'
import { CategoriesService } from './categories.service'
import { CategoryTypesService } from './categoryTypes.service'
import { UserService } from './users.service'
import { PostsService } from './posts.service'
import { Model3DService } from './model3Ds.service'
import { MaterialService } from './materials.service'
import { TextureService } from './textures.service'
import { ProductService } from './products.service'
import { FeaturesService } from '../modules/subscription/features/service'
import { OptionsService } from '../modules/subscription/options/service'
import { ApplicationsService } from '../modules/subscription/applications/service'
import { PlansService } from '../modules/subscription/plans/service'
import { SubscriptionsService } from '../modules/subscription/subscriptions/service'
import { OffersService } from '../modules/subscription/offers/service'
import { BillingsService } from '../modules/billing/service'
import { ProjectsService } from '../modules/project/service'

export default {
  Auth: AuthService,
  CategoriesService,
  CategoryTypesService,
  UserService,
  PostsService,
  Model3DService,
  TextureService,
  MaterialService,
  ProductService,
  FeaturesService,
  OptionsService,
  OffersService,
  ApplicationsService,
  PlansService,
  SubscriptionsService,
  BillingsService,
  ProjectsService
}
