import { BaseService } from './base.service'
import { ResponseWrapper, ErrorWrapper } from './util'
import { assert } from '../index'

export class TextureService extends BaseService {
  static get entity() {
    return 'textures'
  }

  static async getbyCateId(params = {}) {
    assert.id(params.cateId, { require: true })
    try {
      const response = await this.request().get({ cateId: params.cateId })
      const data = {
        content: response.data.data || response.data
      }
      return new ResponseWrapper(response, data)
    } catch (error) {
      throw new ErrorWrapper(error, null)
    }
  }
}
