import { BaseService } from './base.service'
import { ResponseWrapper, ErrorWrapper } from './util'

export class CategoriesService extends BaseService {
  static get entity() {
    return 'categories'
  }

  static async getTypes(parameters = {}) {
    const params = { ...parameters }

    try {
      const response = await this.request().get(`catetypes`, { params })
      const data = {
        content: response.data.data || response.data,
      }
      return new ResponseWrapper(response, data)
    } catch (error) {
      throw new ErrorWrapper(error, null)
    }
  }
}
