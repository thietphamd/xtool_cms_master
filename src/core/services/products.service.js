import { BaseService } from './base.service'
import { ErrorWrapper, ResponseWrapper } from './util'

export class ProductService extends BaseService {
  static get entity() {
    return 'product-management/products'
  }

  static async getList360Requested() {
    try {
      const response = await this.request({ auth: true }).get(`${this.entity}?size=100`)
      return new ResponseWrapper(response, response.data.data)
    } catch (e) {
      return new ErrorWrapper(e)
    }
  }

  static async upload360(data) {
    try {
      const id = data.data.id || data.data.get('id')
      data.data.delete('id')
      const response = await this.request({ auth: true }).post(
        `${this.entity}/${id}/upload`,
        data.data,
        { onUploadProgress: data.updateProgress }
      )
      return new ResponseWrapper(response, response.data.data)
    } catch (e) {
      return new ErrorWrapper(e)
    }
  }
}
