import { BaseService } from './base.service'

export class MaterialService extends BaseService {
  static get entity() {
    return 'materials'
  }
}
