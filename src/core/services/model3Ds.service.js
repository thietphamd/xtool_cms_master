import { BaseService } from './base.service'
import { ResponseWrapper } from './util'
const API_URL = process.env.VUE_APP_BASE_API
export class Model3DService extends BaseService {
  static get entity() {
    return 'models'
  }

  static async adminCreate(data) {
    const path = `${API_URL}models`
    try {
      const response = await this.request().post(path, data)
      return new ResponseWrapper(response, response.data.data)
    } catch (error) {
      return error.response
    }
  }
}
