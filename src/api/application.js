import request from '@/utils/request'
import {API_CMS_BASE} from '../config.js';

const API_APPLICATION = API_CMS_BASE + '/applications';


export function getList(params = {}) {
  return request({
    url: API_APPLICATION,
    method: 'get',
    params: { params }
  })
}

export function detail(id) {
  return request({
    url: API_APPLICATION + `/${id}`,
    method: 'get',
    params: { params }
  })
}

export function create(data) {
  return request({
    url: API_APPLICATION,
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: API_APPLICATION + `/${data._id}`,
    method: 'put',
    data
  })
}

export function destroy(data) {
  return request({
    url: API_APPLICATION + `/${data._id}`,
    method: 'delete'
  })
}