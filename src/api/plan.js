import request from '@/utils/request'
import {API_CMS_BASE} from '../config.js';

const API_PLAN = API_CMS_BASE + '/plans';


export function getList(params = {}) {
  return request({
    url: API_PLAN,
    method: 'get',
    params: { params }
  })
}

export function detail(id) {
  return request({
    url: API_PLAN + `/${id}`,
    method: 'get',
    params: { params }
  })
}

export function create(data) {
  return request({
    url: API_PLAN,
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: API_PLAN + `/${data._id}`,
    method: 'put',
    data
  })
}

export function destroy(data) {
  return request({
    url: API_PLAN + `/${data._id}`,
    method: 'delete'
  })
}

export function updatePlanAddOption(id, data) {
  return request({
    url: API_PLAN + `/${id}/options`,
    method: 'post',
    data
  })
}

export function updatePlanRemoveOption(id, option_id) {
  return request({
    url: API_PLAN + `/${id}/options/${option_id}`,
    method: 'delete'
  })
}