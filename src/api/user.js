import request from '@/utils/request'
import {API_CMS_BASE} from '../config.js';

const API_LOGIN = API_CMS_BASE + '/auth/login';
const API_USERINFO = API_CMS_BASE + '/auth/me';
const API_LOGOUT = API_CMS_BASE + '/auth/logout';

export function login(data) {
  return request({
    url: API_LOGIN,
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: API_USERINFO,
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: API_LOGOUT,
    method: 'post'
  })
}
