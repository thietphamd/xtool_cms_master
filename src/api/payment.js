import request from '@/utils/request'
import {API_CMS_BASE} from '../config.js';

const API_PAYMENT = API_CMS_BASE + '/payments';


export function getList() {
  return request({
    url: API_PAYMENT,
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: API_PAYMENT,
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: API_PAYMENT + `/${data._id}`,
    method: 'put',
    data
  })
}

export function destroy(data) {
  return request({
    url: API_PAYMENT + `/${data._id}`,
    method: 'delete'
  })
}