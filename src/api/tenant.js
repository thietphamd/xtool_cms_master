import request from '@/utils/request'
import {API_CMS_BASE} from '../config.js';

const API_TENANT= API_CMS_BASE + '/tenants';


export function getList() {
  return request({
    url: API_TENANT,
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: API_TENANT,
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: API_TENANT + `/${data._id}`,
    method: 'put',
    data
  })
}

export function destroy(data) {
  return request({
    url: API_TENANT + `/${data._id}`,
    method: 'delete'
  })
}