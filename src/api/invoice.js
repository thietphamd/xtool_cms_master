import request from '@/utils/request'
import requestBinary from '@/utils/requestBinary'
import {API_CMS_BASE} from '../config.js';

const API_INVOICE = API_CMS_BASE + '/invoices';


export function getList() {
  return request({
    url: API_INVOICE,
    method: 'get'
  })
}

export function create(data) {
  return request({
    url: API_INVOICE,
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: API_INVOICE + `/${data._id}`,
    method: 'put',
    data
  })
}

export function destroy(data) {
  return request({
    url: API_INVOICE + `/${data._id}`,
    method: 'delete'
  })
}

export function download(id) {
  return requestBinary({
    url: API_INVOICE + `/${id}` + '/download/pdf',
    method: 'get',
    responseType: 'blob'
  })
}