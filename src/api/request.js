import request from '@/utils/request'
import {API_CMS_BASE} from '../config.js';

const API_REQUEST= API_CMS_BASE + '/requests';


export function getList() {
  return request({
    url: API_REQUEST,
    method: 'get'
  })
}

export function detail(id) {
  return request({
    url: API_REQUEST + `/${id}`,
    method: 'get',
    params: { params }
  })
}

export function updateFinish(data) {
  return request({
    url: API_REQUEST + `/${data._id}` + '/finish',
    method: 'put',
    data
  })
}

export function updateReject(data) {
  return request({
    url: API_REQUEST + `/${data._id}` + '/reject',
    method: 'put',
    data
  })
}

export function destroy(data) {
  return request({
    url: API_REQUEST + `/${data._id}`,
    method: 'delete'
  })
}